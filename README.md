# Accurate Player Overlay Text Example

This shows an example of how to use Accurate Player together with a visual text overlay.

## Setup

1. Enter your license in the file `scripts/license-key.js`.
2. Log in to the Codemill npm repository:

```npm
npm login adduser --registry=https://codemill.jfrog.io/codemill/api/npm/accurate-video/ --scope=@accurate-player
```
3. Run the demo.

## Running the demo

1. `npm install`
2. `npm run start`
3. Open `http://localhost:8080` in your browser.

## Demo

![Text overlay example](overlay-image.png)

[https://demo.accurateplayer.com/text-overlay/](https://demo.accurateplayer.com/text-overlay/)
